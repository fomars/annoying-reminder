# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('task_processor', '0003_auto_20151030_1609'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='reviewed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='task',
            name='expiration_date',
            field=models.DateField(default=datetime.datetime(2015, 11, 9, 18, 47, 50, 237895)),
        ),
    ]
