# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField()),
                ('expiration_date', models.DateField()),
                ('is_complete', models.BooleanField(default=False)),
                ('is_confirmed', models.NullBooleanField(default=None)),
                ('is_planned', models.BooleanField(default=False)),
                ('planned_date', models.DateField()),
                ('is_closed', models.BooleanField(default=False)),
            ],
        ),
    ]
