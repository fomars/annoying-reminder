# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('task_processor', '0002_auto_20151029_1653'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='is_closed',
            new_name='closed',
        ),
        migrations.RenameField(
            model_name='task',
            old_name='is_complete',
            new_name='complete',
        ),
        migrations.RenameField(
            model_name='task',
            old_name='is_confirmed',
            new_name='confirmed',
        ),
        migrations.RenameField(
            model_name='task',
            old_name='is_planned',
            new_name='planned',
        ),
        migrations.AlterField(
            model_name='task',
            name='expiration_date',
            field=models.DateField(default=datetime.datetime(2015, 11, 6, 16, 9, 31, 744698)),
        ),
    ]
