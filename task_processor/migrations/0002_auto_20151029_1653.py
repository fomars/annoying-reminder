# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('task_processor', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='expiration_date',
            field=models.DateField(default=datetime.datetime(2015, 11, 5, 16, 53, 40, 242070)),
        ),
        migrations.AlterField(
            model_name='task',
            name='planned_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
