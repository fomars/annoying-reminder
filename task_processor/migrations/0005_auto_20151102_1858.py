# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('task_processor', '0004_auto_20151102_1847'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='executor',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='task',
            name='inspector',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='expiration_date',
            field=models.DateField(default=datetime.datetime(2015, 11, 9, 18, 58, 58, 311350)),
        ),
    ]
