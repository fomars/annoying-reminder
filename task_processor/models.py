from datetime import datetime, timedelta
from django.db import models

# Create your models here.
from task_processor.services import Notification


class User(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return u'{id}: {name}'.format(id=self.id, name=self.name)


class Task(models.Model):
    executor = models.ForeignKey(User, related_name='task_executor')
    inspectors = models.ManyToManyField(User)
    description = models.TextField()
    expiration_date = models.DateField(default=datetime.today() + timedelta(days=7))
    complete = models.BooleanField(default=False)
    reviewed = models.BooleanField(default=False)
    confirmed = models.NullBooleanField(default=None)
    planned = models.BooleanField(default=False)
    planned_date = models.DateField(null=True, blank=True)
    closed = models.BooleanField(default=False)

    def __unicode__(self):
        return u"Task {id}: {description}, expires on {date}".format(id=self.id,
                                                                     description=self.description,
                                                                     date=self.expiration_date)

    def process(self):
        if self.complete:
            if self.reviewed:
                if self.confirmed:
                    self.close()
                else:
                    self.reopen()
                    self.process_incomplete_task()
            else:
                if self.too_late():
                    self.close()
                else:
                    self.notify_inspector()
        else:
            self.process_incomplete_task()

    def process_incomplete_task(self):
        if self.is_expired():
            self.close()
        else:
            if self.planned:
                if self.is_planned_today_or_earlier():
                    self.notify_executor()
                else:
                    pass
            else:
                self.notify_executor()

    def close(self):
        self.closed = True
        self.save()

    def reopen(self):
        self.complete = False
        self.save()

    def is_expired(self):
        return self.expiration_date.date() < datetime.today().date()

    def too_late(self):
        return datetime.today().date() - self.expiration_date.date() > timedelta(days=1)

    def is_planned_today_or_earlier(self):
        return self.planned_date.date() <= datetime.today().date()

    def notify_inspector(self):
        for inspector in self.inspectors.all():
            self.__send_notification(inspector)

    def notify_executor(self):
        self.__send_notification(self.executor)

    def __send_notification(self, person):
        notification = Notification(person)