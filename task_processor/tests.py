from datetime import datetime, timedelta
from django.test import TestCase
from mock import patch

# Create your tests here.
from task_processor.models import Task, User
from django import setup
setup()


class TaskFlowTests(TestCase):

    def setUp(self):
        self.executor = User()
        self.inspector = User()
        self.executor.save()
        self.inspector.save()

    def test_incomplete_expired(self):
        yesterday = datetime.today() - timedelta(days=1)
        task = Task(executor=self.executor,
                    description='test: incomplete expired',
                    complete=False,
                    closed=False,
                    expiration_date=yesterday)

        task.process()

        self.assertTrue(task.closed)
        self.assertFalse(task.complete)
        self.assertFalse(task.confirmed)

    @patch('task_processor.models.Notification')
    def test_incomplete_active_unplanned(self, mock):
        task = Task(executor=self.executor,
                    description='test: incomplete active unplanned',
                    complete=False,
                    closed=False,
                    expiration_date=datetime.today())

        task.process()

        self.assertTrue(mock.called)

        self.assertFalse(task.closed)
        self.assertFalse(task.confirmed)
        self.assertFalse(task.planned)

    @patch('task_processor.models.Notification')
    def test_incomplete_active_planned_today(self, mock):
        task = Task(executor=self.executor,
                    description='test: incomplete active planned today',
                    complete=False,
                    closed=False,
                    expiration_date=datetime.today() + timedelta(days=2),
                    planned=True,
                    planned_date=datetime.today())

        task.process()

        self.assertTrue(mock.called)

        self.assertFalse(task.closed)
        self.assertFalse(task.confirmed)

    @patch('task_processor.models.Notification')
    def test_incomplete_active_planned_tomorrow(self, mock):
        task = Task(executor=self.executor,
                    description='test: incomplete active planned tomorrow',
                    complete=False,
                    closed=False,
                    expiration_date=datetime.today() + timedelta(days=2),
                    planned=True,
                    planned_date=datetime.today() + timedelta(days=1))

        task.process()

        self.assertFalse(mock.called)

        self.assertFalse(task.closed)
        self.assertFalse(task.confirmed)

    @patch('task_processor.models.Notification')
    def test_incomplete_active_planned_yesterday(self, mock):
        task = Task(executor=self.executor,
                    description='test: incomplete active planned yesterday',
                    complete=False,
                    closed=False,
                    expiration_date=datetime.today() + timedelta(days=2),
                    planned=True,
                    planned_date=datetime.today() - timedelta(days=1))

        task.process()

        self.assertTrue(mock.called)
        self.assertIn(self.executor, mock.call_args[0])

        self.assertFalse(task.closed)
        self.assertFalse(task.confirmed)

    @patch('task_processor.models.Notification')
    def test_complete_not_reviewed_active(self, mock):
        task = Task(executor=self.executor,
                    description='test: complete, not reviewed, active',
                    complete=True,
                    closed=False,
                    expiration_date=datetime.today() - timedelta(days=1))
        task.save()
        task.inspectors.add(self.inspector)

        task.process()

        self.assertTrue(mock.called)
        self.assertIn(str(self.inspector), str(mock.call_args[0]))

        self.assertFalse(task.closed)
        self.assertFalse(task.confirmed)

    def test_complete_not_reviewed_expired(self):
        task = Task(executor=self.executor,
                    description='test: complete, not reviewed, expired',
                    complete=True,
                    closed=False,
                    expiration_date=datetime.today() - timedelta(days=2))
        task.save()
        task.inspectors.add(self.inspector)

        task.process()

        self.assertTrue(task.closed)

    @patch('task_processor.models.Notification')
    def test_complete_reviewed_discarded_active_unplanned(self, mock):
        task = Task(executor=self.executor,
                    description='test: complete, reviewed, discarded, active',
                    complete=True,
                    closed=False,
                    reviewed=True,
                    confirmed=False)

        task.save()
        task.inspectors.add(self.inspector)

        task.process()

        self.assertFalse(task.complete)
        self.assertFalse(task.closed)
        self.assertIn(self.executor, mock.call_args[0])

    def test_complete_reviewed_discarded_expired(self):
        task = Task(executor=self.executor,
                    description='test: complete, reviewed, discarded, expired',
                    complete=True,
                    closed=False,
                    reviewed=True,
                    confirmed=False,
                    expiration_date=datetime.today()-timedelta(days=1))

        task.save()
        task.inspectors.add(self.inspector)

        task.process()

        self.assertFalse(task.complete)
        self.assertTrue(task.closed)

    def test_complete_reviewed_confirmed(self):
        task = Task(executor=self.executor,
                    description='test: complete, reviewed, confirmed',
                    complete=True,
                    closed=False,
                    reviewed=True,
                    confirmed=True,
                    expiration_date=datetime.today())

        task.save()
        task.inspectors.add(self.inspector)

        task.process()

        self.assertTrue(task.complete)
        self.assertTrue(task.closed)


